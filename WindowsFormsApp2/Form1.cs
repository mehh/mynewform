﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        List<Student> studentList = new List<Student>();
        struct Student
        {
            string name;
            string surname;
            int age;
            int[] grades;

            public Student(string name, string surname, int age, int[] grades)
            {
                this.name = name;
                this.surname = surname;
                this.age = age;
                this.grades = grades;
            }

            public string Name { get => name; set => name = value; }
            public string Surname { get => surname; set => surname = value; }
            public int Age { get => age; set => age = value; }
            public int[] Grades { get => grades; set => grades = value; }
            public override string ToString()
            {
                return Name + " " + Surname + "\nGrades: " + String.Join(" ", Grades);
            }
            
        }

        public Form1()
        {
            InitializeComponent();
            Student student1 = new Student("Binno", "Biko", 25, new int[] { 7, 3, 5 });
            Student student2 = new Student("Gary", "Lineker", 21, new int[] { 4, 5, 7 });
            Student student3 = new Student("Jonathan", "Amos", 19, new int[] { 9, 5, 1 });
            studentList.Add(student1);
            studentList.Add(student2);
            studentList.Add(student3);

            foreach(Student stud in studentList)
            {
                listBox1.Items.Add(stud.Name + " " + stud.Surname);
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.listBox1.Visible == true)
            {
                label1.ForeColor = Color.Black;
                int index = listBox1.SelectedIndex;
                int total = 0;
                try
                {
                    foreach (int i in studentList[index].Grades)
                    {
                        total += i;
                    }
                }catch(System.ArgumentOutOfRangeException)
                {
                    return;
                }

                label1.Text += "\nAverage: " + total;
            }else
            {
                button1.Text = "kek";
                label1.Text = "Hello World!";
                label1.ForeColor = Color.Red;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Text = this.comboBox1.GetItemText(this.comboBox1.SelectedItem);
        }

        private void izietToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.comboBox1.Visible = false;
            this.listBox1.Visible = true;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            this.Text = Cursor.Position.X.ToString() + " " + Cursor.Position.Y.ToString();

        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MessageBox.Show("Left button was pressed");
            }
            else if (e.Button == MouseButtons.Right)
            {
                MessageBox.Show("Right button was pressed");
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            // msdn.microsoft.com/en-us/library/system.windows.forms.control.keypress(v=vs.110).aspx
            if (e.KeyCode == Keys.Control) {
                MessageBox.Show("ctrl button was pressed");
            }
            if(e.KeyCode == Keys.Alt)
            {
                MessageBox.Show("alt button was pressed");
            }
            if(e.KeyCode == Keys.Shift)
            {
                MessageBox.Show("shift button was pressed");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            label1.Text = studentList[index].ToString();
        }
    }
}
